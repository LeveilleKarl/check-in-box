package pste235.checkinbox;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karl on 18/03/2017.
 */

public class Box implements Parcelable {

    private final int nbLettre;
    private final int nbColis;
    private final double valPoids;
    //private final int dernierCourrier;
    private final int courrierVoisin;
    private final int courrierPoste;

    public Box() {
        this.nbLettre = 0;
        this.nbColis = 0;
        this.valPoids = 0.0;
        //this.dernierCourrier ;
        this.courrierVoisin = 0;
        this.courrierPoste = 0;
    }

    public Box(JSONObject jObject) {
        this.nbLettre = jObject.optInt("lettre");
        this.nbColis = jObject.optInt("colis");
        this.valPoids = jObject.optDouble("poids");
        //this.dernierCourrier = jObject.optInt("date_reception");
        this.courrierVoisin = jObject.optInt("voisin");
        this.courrierPoste = jObject.optInt("poste");
    }

    public int lettre() {
        return nbLettre;
    }

    public int colis() {
        return nbColis;
    }

    public double poids() {
        return valPoids;
    }

    /*public int date_reception() {
        return dernierCourrier;
    }*/

    public int voisin() {
        return courrierVoisin;
    }

    public int poste() {
        return courrierPoste;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(nbLettre);
        dest.writeInt(nbColis);
        dest.writeDouble(valPoids);
        //dest.writeInt(dernierCourrier);
        dest.writeInt(courrierVoisin);
        dest.writeInt(courrierPoste);
    }

    public static final Parcelable.Creator<Box> CREATOR = new Parcelable.Creator<Box>() {
        @Override
        public Box createFromParcel(Parcel source) {
            return new Box(source);
        }

        @Override
        public Box[] newArray(int size) {
            return new Box[size];
        }
    };

    public Box(Parcel in) {
        this.nbLettre = in.readInt();
        this.nbColis = in.readInt();
        this.valPoids = in.readDouble();
        //this.dernierCourrier = in.readInt();
        this.courrierVoisin = in.readInt();
        this.courrierPoste = in.readInt();
    }



}
