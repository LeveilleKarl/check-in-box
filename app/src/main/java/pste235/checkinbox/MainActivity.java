package pste235.checkinbox;

//import com.TestApp.HelloWebView.R;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    //variables à afficher
    int nbCourrier;
    int nbLettre;
    int nbColis;
    double valPoids;
    Date dernierCourrier;
    int courrierVoisin;
    int courrierPoste;
    String adresse;
    boolean vide;

    //Base de données
    String urlGet = "http://35.187.46.31/connexion.php";
    GetDataAsyncTask getData;
    int success;

    //variables pour affichage
    TextView ncourriers;
    TextView nlettres;
    TextView ncolis;
    TextView npoids;
    TextView date;
    TextView nvoisins;
    TextView nretour;
    TextView adr;

    public void resetData(){
        this.nbCourrier=0;
        this.nbLettre=0;
        this.nbColis=0;
        this.valPoids=0.0f;
        this.dernierCourrier=new Date(0,0,0,0,0);
        this.courrierVoisin=0;
        this.courrierPoste=0;
        this.adresse = " ";
        this.vide=true;
    }

    public void exempleData(){
        this.nbCourrier=3;
        this.nbLettre=2;
        this.nbColis=1;
        this.valPoids=2.59;
        this.dernierCourrier=new Date(2017,2,24,2,5);
        this.courrierVoisin=0;
        this.adresse = "10 rue Sextius Michel 75015 Paris";
        this.courrierPoste=1;
        this.vide=false;
    }
    public void putDataOnDisplay(){
        TextView ncourriers = (TextView) findViewById(R.id.ncourriers);
        TextView nlettres = (TextView) findViewById(R.id.nlettres);
        TextView ncolis = (TextView) findViewById(R.id.ncolis);
        TextView npoids = (TextView) findViewById(R.id.npoids);
        TextView date = (TextView) findViewById(R.id.date);
        TextView nvoisins = (TextView) findViewById(R.id.nbvoisins);
        TextView nretour = (TextView) findViewById(R.id.nretour);
        TextView adr = (TextView) findViewById(R.id.adresse);

        ncourriers.setText((Integer.toString(this.nbCourrier)));
        nlettres.setText(Integer.toString(this.nbLettre));
        ncolis.setText(Integer.toString(this.nbColis));
        npoids.setText(Double.toString(this.valPoids)+" kg");
        date.setText(this.dernierCourrier.getString());
        nvoisins.setText(Integer.toString(this.courrierVoisin));
        nretour.setText(Integer.toString(this.courrierPoste));
        adr.setText(adresse);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setContentView(R.layout.activity_main); //pour affecter une valeur

        resetData();
        putDataOnDisplay();
        ncourriers = (TextView) findViewById(R.id.ncourriers);
        nlettres = (TextView) findViewById(R.id.nlettres);
        ncolis = (TextView) findViewById(R.id.ncolis);
        npoids = (TextView) findViewById(R.id.npoids);
        date = (TextView) findViewById(R.id.date);
        nvoisins = (TextView) findViewById(R.id.nbvoisins);
        nretour = (TextView) findViewById(R.id.nretour);
        nretour = (TextView) findViewById(R.id.adresse);
        getData=new GetDataAsyncTask();
        getData.execute(urlGet);

        final ImageButton acc = (ImageButton) findViewById(R.id.imageButton);
        acc.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                GetDataAsyncTask Acc=new GetDataAsyncTask();
                Acc.execute(urlGet);
            }
        });
        final Button histo = (Button) findViewById(R.id.button);
        histo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Bundle objetbunble = new Bundle();
                Relevé[] liste = getListeRelevé();
                boolean[] listeB = new boolean[liste.length];
                long[] listeL = new long[liste.length];
                for(int i=0;i<liste.length;i++){
                    listeB[i]=liste[i].isCourier;
                    listeL[i]=Long.decode(liste[i].récupéré.getCode());
                }
                objetbunble.putBooleanArray("isCourrier",listeB);
                objetbunble.putLongArray("récupéré",listeL);
                Intent intent = new Intent(MainActivity.this, Historique.class);
                intent.putExtras(objetbunble);

                startActivity(intent);
            }
        });

    }

    public Relevé[] getListeRelevé(){
        Relevé liste[] = new Relevé[6];
        liste[0]=new Relevé(true,new Date(2017,5,5,02,01));
        liste[1]=new Relevé(false,new Date(2017,12,28,03,35));
        liste[2]=new Relevé(true,new Date(2017,9,03,11,58));
        liste[3]=new Relevé(true,new Date(2017,5,5,02,01));
        liste[4]=new Relevé(false,new Date(2017,12,28,03,35));
        liste[5]=new Relevé(true,new Date(2017,9,03,11,58));
        return liste;
    }

    private class GetDataAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            Log.i("add", " start doInBackground");
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("GET");
                connection.setDoInput(true);
                connection.connect();
                InputStream is = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(is));

                StringBuilder builder = new StringBuilder();

                String line = "" ;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
                return builder.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if(connection != null) {
                    connection.disconnect();
                }
                try {
                    if(reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Log.i("add", " end doInBackground");
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.i("add", "onPostExecute");
            super.onPostExecute(result);
            TextView lettre = (TextView)findViewById(R.id.nlettres);
            TextView colis = (TextView)findViewById(R.id.ncolis);
            TextView poids = (TextView)findViewById(R.id.npoids);
            TextView date = (TextView)findViewById(R.id.date);
            TextView voisin = (TextView)findViewById(R.id.nbvoisins);
            TextView poste = (TextView)findViewById(R.id.nretour);
            TextView courrier = (TextView)findViewById(R.id.ncourriers);
            TextView adresse = (TextView)findViewById(R.id.adresse);
            String jsonStr = result;
            if(jsonStr != null)
            {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    success = jsonObj.getInt("success");
                    if (success == 1)
                    {
                        Toast.makeText(getApplicationContext(), "Synchronisation réussie ", Toast.LENGTH_LONG).show();
                        JSONArray dataValues = jsonObj.getJSONArray("valeurs");
                        // loop each row in the array
                        for (int j = 0; j < dataValues.length(); j++) {
                            JSONObject values = dataValues.getJSONObject(j);
                            String l = values.getString("lettre");
                            String c = values.getString("colis");
                            String p = values.getString("poids");
                            String d = values.getString("date_reception");
                            String v = values.getString("voisin");
                            String retour = values.getString("poste");
                            String adr = values.getString("adresse");
                            //add a string witch contains all of data getted from the response
                            if (lettre != null) {
                                lettre.setText(l);
                            }
                            if (colis != null) {
                                colis.setText(c);
                            }
                            if (poids != null) {
                                poids.setText(p +" kg");
                            }
                            if (date != null) {
                                date.setText(d);
                            }
                            if (voisin != null) {
                                voisin.setText(v);
                            }
                            if (poste != null) {
                                poste.setText(retour);
                            }
                            if (adresse != null) {
                                adresse.setText(adr);
                            }
                            if((lettre!=null)||(colis!=null))
                            {
                                //calcul et affichage du nombre de courrier
                                int nl = values.optInt("lettre");
                                int nc = values.optInt("colis");
                                int cour = nl + nc;
                                courrier.setText((Integer.toString(cour)));
                            }
                        }
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Erreur de synchronisation", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
