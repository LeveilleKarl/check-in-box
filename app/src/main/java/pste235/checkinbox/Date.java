package pste235.checkinbox;

public class Date {
    int année,mois,jour,heur,minute;
    boolean exist;
    public Date(int a,int m,int j,int h,int min){
        this.année=a;
        this.mois=m;
        this.jour=j;
        this.heur=h;
        this.minute=min;
        this.exist=true;
    }
    public Date(){this.exist=false;}

    public Date max(Date d1,Date d2){
        Date max = d1;
        if(Integer.getInteger(d1.getCode())<Integer.getInteger(d2.getCode()))max=d2;
        return max;
    }

    public Date min(Date d1,Date d2){
        Date min = d1;
        if(Integer.getInteger(d1.getCode())>Integer.getInteger(d2.getCode()))min=d2;
        return min;
    }

    public String getString(){
        if(this.exist){
            String str = new String("Le : ");
            if(this.jour<10)str=str+"0";str=str+this.jour+"/";
            if(this.mois<10)str=str+"0";str=str+this.mois+"/";
            str=str+this.année;
            str=str+" à ";
            if(this.heur<10)str=str+"0";str=str+this.heur+"h";
            if(this.minute<10)str=str+"0";str=str+this.minute;
            return str;
        }else{
            return "Null";
        }
    }

    public String getCode(){
        if(this.exist){
            String str = new String();
            str=str+this.année;
            if(this.mois<10)str=str+"0";str=str+this.mois;
            if(this.jour<10)str=str+"0";str=str+this.jour;
            if(this.heur<10)str=str+"0";str=str+this.heur;
            if(this.minute<10)str=str+"0";str=str+this.minute;
            return str;
        }else{
            return "Null";
        }

    }

}
