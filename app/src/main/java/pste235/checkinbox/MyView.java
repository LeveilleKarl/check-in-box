package pste235.checkinbox;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.support.v4.app.NavUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Sovandara on 19/04/2017.
 */

public class MyView extends View {

    private final static String TEST_STRING = "Historique";
    private final static String OBJET = "Objet : ";
    private final static String DATE = "Reçu le  : ";
    private final static String HEURE = "À : ";
    private Paint textHisto;
    private Paint textObjet;
    Paint paint;
    Paint histo;
    int nbticket;
    int offset=0;
    Bitmap colis;
    Bitmap lettre;
    Rect dst = new Rect();
    boolean[] listeB;
    long[] listeL;
    int posX=0;
    int posY=0;

    public MyView(Context context,boolean[] _listeB,long[] _listeL,int _offset) {
        super(context);
        //offset=_offset;
        init();
        nbticket=_listeB.length;
        listeB=_listeB;
        listeL=_listeL;
        InputStream input = null;
        InputStream inputStream = null;
        try {
            Log.i("colis", "Récupération asset");
            Log.i("lettre", "Récupération asset");
            AssetManager assetManager = context.getAssets();
            inputStream = assetManager.open("colis.png");
            input = assetManager.open("lettre.jpg");
            colis = BitmapFactory.decodeStream(inputStream);
            lettre = BitmapFactory.decodeStream(input);
            Log.d("colis", "Format du png: " + colis.getConfig());
            Log.d("lettre", "Format du png: " + colis.getConfig());

        } catch (IOException e) {
            e.printStackTrace();
            Log.e("colis", "Le fichier n'a pas été trouvé !");
            Log.e("lettre", "Le fichier n'a pas été trouvé !");
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
                if (input != null)
                    input.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    public MyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        init();
    }


    private void init() {
        paint = new Paint();
        paint.setColor(0x2025D6FF);
        paint.setStrokeWidth(1);
        paint.setStyle(Paint.Style.STROKE);

        histo = new Paint();
        histo.setColor(0xFF68D736);
        histo.setStrokeWidth(1);
        histo.setStyle(Paint.Style.STROKE);

        textHisto = new Paint(Paint.ANTI_ALIAS_FLAG);
        textHisto.setStyle(Paint.Style.FILL_AND_STROKE);
        textHisto.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        textHisto.setTextSize(90.0f);

        textObjet = new Paint(Paint.ANTI_ALIAS_FLAG);
        textObjet.setStyle(Paint.Style.FILL_AND_STROKE);
        textObjet.setTextSize(50.0f);

        offset=0;
    }


    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        int savedCount = canvas.save();
        drawRectangle(canvas);
        canvas.restoreToCount(savedCount);
    }


    protected void drawRectangle(Canvas canvas) {

        final RectF rect = new RectF();
        Bitmap btm;

        Log.i("colis", "onDraw");
        Log.i("lettre", "onDraw");

        for (int i = 0; i < nbticket; i++){
            rect.set(10, 150 + i*canvas.getHeight()/5+offset, 1070, (i+1)*canvas.getHeight()/5+120+offset);
            canvas.drawRoundRect(rect, 60, 60, paint);
            String str1 = OBJET;
            if(listeB[i]){
                str1=str1+"Courrier";
                btm=lettre;
            }else{
                str1=str1+"Colis";
                btm=colis;
            }
            String date = Long.toString(listeL[i]);
            String str2 = DATE+date.substring(6,8)+"/"+date.substring(4,6)+"/"+date.substring(0,4);
            String str3 = HEURE +date.substring(8,10)+"h"+date.substring(10,12);
            canvas.drawText(str1, 400, (i+1)*canvas.getHeight()/5-120+offset, textObjet);
            canvas.drawText(str2, 400, (i+1)*canvas.getHeight()/5-20+offset, textObjet);
            canvas.drawText(str3, 400, (i+1)*canvas.getHeight()/5+80+offset, textObjet);
            dst.set(50, (i+1)*canvas.getHeight()/5-160+offset, 300, (i+1)*canvas.getHeight()/5+100+offset);
            canvas.drawBitmap(btm, null, dst, null);
        }
        paint.setStyle(Paint.Style.FILL);
        histo.setStyle(Paint.Style.FILL);
        rect.set(0, 0, canvas.getWidth(), 130);
        canvas.drawRoundRect(rect, 0, 0, histo);
        Rect text = new Rect();
        textHisto.getTextBounds(TEST_STRING, 0, 1, text);
        canvas.drawText(TEST_STRING, (float) (canvas.getWidth() / 3.2), 100, textHisto);

    }
    public boolean onTouchEvent(MotionEvent e)
    {

        int xpos=(int) e.getX();
        int ypos=(int) e.getY();
        if(ypos-posY<0){
            offset=offset-30;
        }else{
            offset=offset+30;
        }
        if(offset>0)offset=0;

        this.invalidate();
        posX=xpos;
        posY=ypos;
        return true;

    }

}
